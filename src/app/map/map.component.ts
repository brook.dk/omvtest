import { Component, OnInit } from '@angular/core';
declare let L;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {
  constructor() {

  }
  ngOnInit() {
    const api_key = 'cb174e9c-121e-435f-9dde-8eb8b27db714'
    const center = [59.9139, 10.7522]
    // Adding the map to the div with id 'map' centering on latitude 60 and longitude 10
    const map = L.map('map').setView(center, 15);

    // This is the map types norkart currently support the admin will be able to configure which are visible
    const grey = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.GREY});
    const vector = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.VECTOR});
    const aerial = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.AERIAL});
    const hybrid = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.HYBRID});
    const medium = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.MEDIUM});
    const lite = L.tileLayer.webatlas({apikey: api_key, mapType: L.TileLayer.Webatlas.Type.LITE});
    let baseMaps = {
      "grey": grey,
      "vector": vector,
      "aerial": aerial,
      "hybrid": hybrid,
      "medium": medium,
      "lite": lite,
    };
    var overlayMaps = {
    };
    // To be replace with your prefered default map
    vector.addTo(map);
    
    L.control.layers(baseMaps, overlayMaps).addTo(map);   // This is to allow the user to be able to change the maptypes
    
    // Initialise the FeatureGroup to store editable layers
    var editableLayers = new L.FeatureGroup();
    map.addLayer(editableLayers);
  
    var options = {
      position: 'topleft',
      draw: {
        polyline: {
          shapeOptions: {
            color: '#f357a1',
            weight: 3
          }
        },
      // disable toolbar item by setting it to false
      circle: false, // Turns off this drawing tool
      polygon: false,
      marker: false,
      rectangle: false,
      circlemarker:false,
    },
    edit: {
      featureGroup: editableLayers, //REQUIRED!!
      edit: false,
      remove: true,
    },
  };
  // Initialise the draw control and pass it the FeatureGroup of editable layers
  var drawControl = new L.Control.Draw(options);
  map.addControl(drawControl);
  
  map.on('draw:created', function(e) {
    var type = e.layerType,
    layer = e.layer;
    editableLayers.addLayer(layer);
        ///TODO: Get affected owners list from Norkart
});
  }
  title = 'angular-leaflet';
}
