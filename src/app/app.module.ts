import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClient } from '@angular/common/http';
//import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { FormsModule } from '@angular/forms';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { ToastrModule } from 'ngx-toastr';
import { AuthModule, ConfigResult, OidcConfigService, OidcSecurityService, OpenIdConfiguration } from 'angular-auth-oidc-client';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http'
import { MapComponent } from './map/map.component';
import { NavComponent } from './nav/nav.component';
import { ApplicationComponent } from './application/application.component';
import { ApplicationFormComponent } from './application-form/application-form.component';
import { ReactiveFormsModule } from '@angular/forms';
const oidc_configuration = 'assets/auth.clientConfiguration.json'; //'assets/auth.clientConfiguration.local.json';
export function loadConfig(oidcConfigService: OidcConfigService) {
   return () => oidcConfigService.load(oidc_configuration);
 }
@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    NavComponent,
    ApplicationComponent,
    ApplicationFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AuthModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfig,
      deps: [OidcConfigService],
      multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
   private oidcSecurityService: OidcSecurityService, private oidcConfigService: OidcConfigService) {
     this.oidcConfigService.onConfigurationLoaded.subscribe((configResult: ConfigResult) => {

     const config: OpenIdConfiguration = {
       stsServer: 'https://dimeidentityserver.azurewebsites.net',
       redirect_url: 'https://localhost:4200',
       post_logout_redirect_uri: 'https://localhost:4200',
       post_login_route: 'parent/children',
       client_id: '361e78fc-aa26-469a-b56e-5cffec63715b',
       scope: 'openid profile bhgapp',
       response_type: 'code',
       silent_renew: true,
       silent_renew_url: 'https://localhost:4200/silent-renew.html',
       log_console_debug_active: false,
       max_id_token_iat_offset_allowed_in_seconds: 10
     };
     this.oidcSecurityService.setupModule(config, configResult.authWellknownEndpoints);
     });
 }
}


