import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { AuthenticatedUser } from '../models/authenticated-user.model';
// import { StateService } from '../services/state.service';

import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpParams } from '@angular/common/http';
// import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // authenticatedUser: AuthenticatedUser;
  isAuthenticated: boolean;
  userData: any;
  person: any;

 constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    // private stateService: StateService,
    public oidcSecurityService: OidcSecurityService,
    // private peopleService: PeopleService
  ) {
  }

  ngOnInit() {
    console.log('ngOnInit 1');

    this.oidcSecurityService.getIsAuthorized().subscribe(auth => {
      this.isAuthenticated = auth;

      if (this.isAuthenticated) {
        console.log('user is authenticated');
        // this.getUserData();
      }
    });
  }

  // getUserData() {
  //   this.oidcSecurityService.getUserData().subscribe(userData => {
  //     this.userData = userData;
  //     console.log('userdata: ' + this.userData.pid);
  //     if (this.userData !== undefined && this.userData.pid) {
  //       console.log('user data is defined');
  //       console.log(this.userData);
  //       this.authenticatedUser = {
  //               parentId: this.userData.pid,  // 19128600658,5027100165 //11127202917
  //               childMunicipalityId: 0,
  //               childAddress: '',
  //               parentName: '',
  //               isAuthenticated: true
  //             };
  //       this.stateService.setCurrentUser(this.authenticatedUser);
  //       console.log(this.authenticatedUser);

  //       this.router.navigate(['/parent/children']);
  //     }
  //   });
  // }

  login() {
    this.oidcSecurityService.authorize();
  }

  logout() {
   this.oidcSecurityService.logoff();
  }
}
