import { Injectable } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms'
import { Observable, of } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { Application } from '../models/application';
@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  private relationships: Array<any> = [
    {id: 1, name: 'Partner'},
    {id: 2, name: 'Child'},
    {id: 3, name: 'Parent'},
    {id: 4, name: 'Sibling'}
  ];
  private reasons: Array<any> = [
    {id: 1, name: 'Reason 1'},
    {id: 2, name: 'Reason 2'},
    {id: 3, name: 'Reason 3'},
    {id: 4, name: 'Reason 4'}
  ];
formData:Application;
  constructor(private http:HttpClient) {

  }
  //Here we call the APi method addApplicationForm
readonly APIUrl="https://localhost:44351/api/application";
addApplicationForm(application:Application){
  return this.http.post(this.APIUrl,application)
}
  getAllRelationships(): Observable<any[]> {
    return of(this.relationships);
  }
  getAllReasons(): Observable<any[]> {
    return of(this.reasons);
  }
}
