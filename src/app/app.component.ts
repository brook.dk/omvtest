// import { AppService } from 'src/app/services/app.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { OnInit, Component } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Inhabitants Application';
  showHeader = false;
  showSidebar = false;
  showFooter = false;
  isAuthenticated: boolean;
  constructor(
    // private appService: AppService,
    private router: Router,
    private activatedRoute: ActivatedRoute,

    public oidcSecurityService: OidcSecurityService,) 
    {

    if (this.oidcSecurityService.moduleSetup) {
      this.doCallbackLogicIfRequired();
    } else {
      this.oidcSecurityService.onModuleSetup.subscribe(() => {
        this.doCallbackLogicIfRequired();
      });
    }
              }

              ngOnInit() {
                this.router.events.subscribe(event => {
                  if (event instanceof NavigationEnd) {
                    this.showHeader = this.activatedRoute.firstChild.snapshot.data.showHeader !== false;
                    this.showSidebar = this.activatedRoute.firstChild.snapshot.data.showSidebar !== false;
                    this.showFooter = this.activatedRoute.firstChild.snapshot.data.showFooter !== false;
                  }
                });
              }
  private doCallbackLogicIfRequired() {
    this.oidcSecurityService.authorizedCallbackWithCode(window.location.toString());
  }
}
