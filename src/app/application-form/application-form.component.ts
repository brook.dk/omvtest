import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../services/application.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Application } from '../models/application';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css']
})
export class ApplicationFormComponent implements OnInit {
  public applicationForm = this.fb.group({});
  public relationships = this.service.getAllRelationships();
  public reasons = this.service.getAllReasons();

  // private additionalDrivers:Observable<string[]> = Som
  formSubmitted = false;
  // tslint:disable-next-line: whitespace
  constructor(public service: ApplicationService, private fb: FormBuilder) {}

  ngOnInit() {
    this.applicationForm.addControl(
      'additionalDriverId',
      new FormControl('', [Validators.required])
    );
    this.applicationForm.addControl(
      'additionalDriverRelation',
      new FormControl('', [Validators.required])
    );
    this.applicationForm.addControl(
      'vehicletype',
      new FormControl('1', [Validators.required])
    );
    this.applicationForm.addControl(
      'time',
      new FormControl('1', [Validators.required])
    );
    this.applicationForm.addControl(
      'reason',
      new FormControl('', [Validators.required])
    );
    this.applicationForm.addControl(
      'confirmation',
      new FormControl('', [Validators.required])
    );
    this.applicationForm.addControl('remark', new FormControl(''));
  }

  public saveApplication() {
    // create a new model instance
    const application = new Application();
    application.additionalDriverId = this.applicationForm.get(
      'additionalDriverId'
    ).value;
    application.additionalDriverRelation = this.applicationForm.get(
      'additionalDriverRelation'
    ).value;
    application.vehicletype = this.applicationForm.get(
      'vehicletype'
    ).value;
    application.time = this.applicationForm.get('time').value;
    application.reason = this.applicationForm.get('reason').value;
    application.confirmation = this.applicationForm.get(
      'confirmation'
    ).value;
    application.remark = this.applicationForm.get('remark').value;
    // model.property = this.applicationForm.get('property').value;
    // pass the instance to ApplicationService and save
    //console.log(application );
    //Here is the added method to send data to the backend as a service
    this.service.addApplicationForm(this.applicationForm.value).subscribe(res=>
      {
        alert(res);
      }

      )
  }
}
